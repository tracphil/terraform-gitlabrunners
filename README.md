# Analytics

Deploy new instances on stack.naturalis.io with Terraform

# FLow

1. Initialize the terraform repo
    ```bash
    cd terraform
    terraform init
    ```

# Terraform

## Localy
This will install a small instance with a security group and a DNS record of `{service_name}-development-main.hosts.naturalis.io`. It will output the DNS name and the IP address of the instance

```bash
cd terraform
terraform fmt
terraform validate
terraform plan
terraform apply -auto-approve
```

### Authorization (e.g.)
* AWS: ~/.aws/credentials
* Stack: ~/.config/openstack/clouds.yaml
  * En zet daar de juiste variablen

## Gitlab CICD
### Authorization
* Geen credentials verwijzing voor 'provider "openstack"' in de main.tf
* Credentials in Gitlab Settings/CI CD/Variables
  * AWS_ACCESS_KEY_ID
  * AWS_SECRET_ACCESS_KEY
  * OS_AUTH_URL
  * OS_IDENTITY_API_VERSION
  * OS_INTERFACE
  * OS_PASSWORD
  * OS_PROJECT_ID
  * OS_PROJECT_NAME
  * OS_REGION_NAME
  * OS_USER_DOMAIN_NAME
  * OS_USERNAME
  * TF_VAR_service
  * TF_VAR_service_team

### Run
as in .gitlab-ci.yml
```yaml
<...>
Terraform plan:
  stage: Plan
  environment:
    name: ${CI_COMMIT_BRANCH}
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform init
    - gitlab-terraform plan -var="gitlab_project_url=$CI_PROJECT_URL" -var="environment=$CI_ENVIRONMENT_NAME"
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(development|production)$/'

Terraform apply:
  stage: Deploy
  environment:
    name: ${CI_COMMIT_BRANCH}
  script:
    - cd ${TF_ROOT}
    - gitlab-terraform apply
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(development|production)$/'
<...>
```

