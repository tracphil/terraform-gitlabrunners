terraform {
  backend "http" {}
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "openstack" {}

provider "aws" {
  region = "eu-west-1"
}

locals {
  prefix = "${lookup(var.envprefix, var.environment)}-${var.service}"
  common_tags = {
    environment   = var.environment
    service       = var.service
    service_owner = var.service_owner
    service_team  = var.service_team
    managed_by    = var.gitlab_project_url
    project       = var.project
  }
  keypair_name = "service-${var.service}-deploy"
}
