# Create a network port for each instance to deploy and add it to the netwsork {} part of your instance. Security groups are added to this port

resource "openstack_networking_port_v2" "port_gitlabrunner_005" {
  admin_state_up = "true"
  network_id     = data.openstack_networking_network_v2.instance_network.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.default_sg.id,
    data.openstack_networking_secgroup_v2.vault_access.id
  ]
}

resource "openstack_networking_port_v2" "port_gitlabrunner_006" {
  admin_state_up = "true"
  network_id     = data.openstack_networking_network_v2.instance_network.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.default_sg.id,
    data.openstack_networking_secgroup_v2.vault_access.id
  ]
}

resource "openstack_networking_port_v2" "port_gitlabrunner_007" {
  admin_state_up = "true"
  network_id     = data.openstack_networking_network_v2.instance_network.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.default_sg.id,
    data.openstack_networking_secgroup_v2.vault_access.id
  ]
}

resource "openstack_networking_port_v2" "port_gitlabrunner_008" {
  admin_state_up = "true"
  network_id     = data.openstack_networking_network_v2.instance_network.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.default_sg.id,
    data.openstack_networking_secgroup_v2.vault_access.id
  ]
}

resource "openstack_networking_port_v2" "port_gitlabrunner_009" {
  admin_state_up = "true"
  network_id     = data.openstack_networking_network_v2.instance_network.id
  security_group_ids = [
    data.openstack_networking_secgroup_v2.default_sg.id,
    data.openstack_networking_secgroup_v2.vault_access.id
  ]
}

