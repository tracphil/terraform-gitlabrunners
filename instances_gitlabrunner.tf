# Gitlabrunner 005

resource "openstack_compute_instance_v2" "gitlabrunner-005" {
  name        = "${local.prefix}-005"
  image_name  = var.ubuntu_image_name
  flavor_name = "large-4c.8r.20h"
  user_data   = file("./templates/default/cloud-init.yml")
  metadata    = local.common_tags

  network {
    port = openstack_networking_port_v2.port_gitlabrunner_005.id
  }
  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_blockstorage_volume_v3" "gitlabrunner-005" {
  name                 = "${local.prefix}-005-data_volume_1"
  enable_online_resize = true
  size                 = var.default_volume_size
  metadata             = local.common_tags
}

resource "openstack_compute_volume_attach_v2" "gitlabrunner-005-attach" {
  instance_id = openstack_compute_instance_v2.gitlabrunner-005.id
  volume_id   = openstack_blockstorage_volume_v3.gitlabrunner-005.id
}

# Gitlabrunner 006

resource "openstack_compute_instance_v2" "gitlabrunner-006" {
  name        = "${local.prefix}-006"
  image_name  = var.ubuntu_image_name
  flavor_name = var.default_instance_flavor
  user_data   = file("./templates/default/cloud-init.yml")
  metadata    = local.common_tags

  network {
    port = openstack_networking_port_v2.port_gitlabrunner_006.id
  }
  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_blockstorage_volume_v3" "gitlabrunner-006" {
  name                 = "${local.prefix}-006-data_volume_1"
  enable_online_resize = true
  size                 = var.default_volume_size
  metadata             = local.common_tags
}

resource "openstack_compute_volume_attach_v2" "gitlabrunner-006-attach" {
  instance_id = openstack_compute_instance_v2.gitlabrunner-006.id
  volume_id   = openstack_blockstorage_volume_v3.gitlabrunner-006.id
}

# Gitlabrunner 007

resource "openstack_compute_instance_v2" "gitlabrunner-007" {
  name        = "${local.prefix}-007"
  image_name  = var.ubuntu_image_name
  flavor_name = var.default_instance_flavor
  user_data   = file("./templates/default/cloud-init.yml")
  metadata    = local.common_tags

  network {
    port = openstack_networking_port_v2.port_gitlabrunner_007.id
  }
  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_blockstorage_volume_v3" "gitlabrunner-007" {
  name                 = "${local.prefix}-007-data_volume_1"
  enable_online_resize = true
  size                 = var.default_volume_size
  metadata             = local.common_tags
}

resource "openstack_compute_volume_attach_v2" "gitlabrunner-007-attach" {
  instance_id = openstack_compute_instance_v2.gitlabrunner-007.id
  volume_id   = openstack_blockstorage_volume_v3.gitlabrunner-007.id
}

# Gitlabrunner 008

resource "openstack_compute_instance_v2" "gitlabrunner-008" {
  name        = "${local.prefix}-008"
  image_name  = var.ubuntu_image_name
  flavor_name = var.default_instance_flavor
  user_data   = file("./templates/default/cloud-init.yml")
  metadata    = local.common_tags

  network {
    port = openstack_networking_port_v2.port_gitlabrunner_008.id
  }
  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_blockstorage_volume_v3" "gitlabrunner-008" {
  name                 = "${local.prefix}-008-data_volume_1"
  enable_online_resize = true
  size                 = var.default_volume_size
  metadata             = local.common_tags
}

resource "openstack_compute_volume_attach_v2" "gitlabrunner-008-attach" {
  instance_id = openstack_compute_instance_v2.gitlabrunner-008.id
  volume_id   = openstack_blockstorage_volume_v3.gitlabrunner-008.id
}

# Gitlabrunner 009

resource "openstack_compute_instance_v2" "gitlabrunner-009" {
  name        = "${local.prefix}-009"
  image_name  = var.ubuntu_image_name
  flavor_name = var.default_instance_flavor
  user_data   = file("./templates/default/cloud-init.yml")
  metadata    = local.common_tags

  network {
    port = openstack_networking_port_v2.port_gitlabrunner_009.id
  }
  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "openstack_blockstorage_volume_v3" "gitlabrunner-009" {
  name                 = "${local.prefix}-009-data_volume_1"
  enable_online_resize = true
  size                 = var.default_volume_size
  metadata             = local.common_tags
}

resource "openstack_compute_volume_attach_v2" "gitlabrunner-009-attach" {
  instance_id = openstack_compute_instance_v2.gitlabrunner-009.id
  volume_id   = openstack_blockstorage_volume_v3.gitlabrunner-009.id
}


