resource "aws_route53_record" "gitlabrunner-005" {
  zone_id = data.aws_route53_zone.hosts.zone_id
  name    = "${openstack_compute_instance_v2.gitlabrunner-005.name}.${data.aws_route53_zone.hosts.name}"
  type    = "A"
  ttl     = "300"
  records = [openstack_compute_instance_v2.gitlabrunner-005.access_ip_v4]
}

resource "aws_route53_record" "gitlabrunner-006" {
  zone_id = data.aws_route53_zone.hosts.zone_id
  name    = "${openstack_compute_instance_v2.gitlabrunner-006.name}.${data.aws_route53_zone.hosts.name}"
  type    = "A"
  ttl     = "300"
  records = [openstack_compute_instance_v2.gitlabrunner-006.access_ip_v4]
}

resource "aws_route53_record" "gitlabrunner-007" {
  zone_id = data.aws_route53_zone.hosts.zone_id
  name    = "${openstack_compute_instance_v2.gitlabrunner-007.name}.${data.aws_route53_zone.hosts.name}"
  type    = "A"
  ttl     = "300"
  records = [openstack_compute_instance_v2.gitlabrunner-007.access_ip_v4]
}

resource "aws_route53_record" "gitlabrunner-008" {
  zone_id = data.aws_route53_zone.hosts.zone_id
  name    = "${openstack_compute_instance_v2.gitlabrunner-008.name}.${data.aws_route53_zone.hosts.name}"
  type    = "A"
  ttl     = "300"
  records = [openstack_compute_instance_v2.gitlabrunner-008.access_ip_v4]
}

resource "aws_route53_record" "gitlabrunner-009" {
  zone_id = data.aws_route53_zone.hosts.zone_id
  name    = "${openstack_compute_instance_v2.gitlabrunner-009.name}.${data.aws_route53_zone.hosts.name}"
  type    = "A"
  ttl     = "300"
  records = [openstack_compute_instance_v2.gitlabrunner-009.access_ip_v4]
}
