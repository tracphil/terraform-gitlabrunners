variable "service" {
  description = "Name of the service for this deployment. Used to prefix all resources. Set TF_VAR_service_name in the pipeline"
  default     = "gitlabrunner"
}

variable "service_owner" {
  description = "Name of the service owner for this service (optional)"
  default     = "Arjan Schieven"
}

variable "service_team" {
  description = "Name of the support team for this service (optional)"
  default     = "infra"
}

variable "project" {
  description = "Project name for this service (optional)"
  default     = "gitlabrunner"
}

variable "gitlab_project_url" {
  description = "URL of the gitlab URL"
  default     = "https://gitlab.com/naturalis/core/gitlabrunner/terraform-gitlabrunner"
}

variable "environment" {
  description = "Name of the environment being deployed."
}

variable "envprefix" {
  description = "prefix per environment"
  type        = map(string)
  default = {
    production  = "prd"
    acceptance  = "acc"
    development = "dev"
  }
}

variable "ubuntu_image_name" {
  description = "Openstack Ubuntu Image name"
  default     = "ubuntu-20.04.2-20210222"
}

variable "default_instance_flavor" {
  description = "Default instance size for runners"
  default     = "medium-2c.4r.20h"
}

variable "default_volume_size" {
  description = "Default volume size for runners"
  default     = 80
}

variable "instance_network" {
  description = "The name of the network to connect the network port to"
  type        = string
  default     = "public-1"
}

variable "tenant_id" {
  description = "Openstack ID of the project"
  type        = string
}
