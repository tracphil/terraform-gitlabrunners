data "aws_route53_zone" "hosts" {
  name = "hosts.naturalis.io."
}

data "aws_region" "current" {}

data "openstack_networking_secgroup_v2" "vault_access" {
  name = "shared_sg_vault_access"
}

# Make sure default_security_group_id variable is set to the default security group ID
data "openstack_networking_secgroup_v2" "default_sg" {
  tenant_id = var.tenant_id
  name      = "default"
}
data "openstack_networking_network_v2" "instance_network" {
  name = var.instance_network
}

